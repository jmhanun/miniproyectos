#!/usr/bin/python
# -*- coding: utf-8 -*-

from pprint import pprint
import xlrd
import time

def list_powerset(lst):
    result = [[]]
    for x in lst:
        result.extend([subset + [x] for subset in result])
    return result


cheques = []
cheques_pago = []
suma_pago = 0
ERROR = 0

book = xlrd.open_workbook("lista_cheques.xls")

sheet = book.sheet_by_name("Sheet1")

print
monto_pago = sheet.row_values(0)[5]
mensaje = "Ingrese monto(default=%s): " % round(monto_pago, 2)
x = raw_input(mensaje)
if x and x != 0:
    monto_pago = float(x)

y = raw_input("Ingrese el error tolerado(default=0): ")
if y and y != 0:
    ERROR = float(y)

#Carga cheques
montos = []
cheques = {}
for row in range(sheet.nrows):
#    cheque["monto"] = sheet.row_values(row)[1]
#    cheque["orden"] = int(sheet.row_values(row)[0])
#    cheque["nro_interno"] = str(sheet.row_values(row)[2])
#    cheques.append(cheque)
    cheques[str(sheet.row_values(row)[2])] = sheet.row_values(row)[1]
    montos.append(sheet.row_values(row)[1])

montos = sorted(montos)
#Fin carga cheques

start = time.time()
pprint (cheques)
pprint (montos)

print "-"*15
eliminar_cheque = list(cheques.keys())[list(cheques.values()).index(3000.0)]
print cheques.pop(eliminar_cheque)
print "-"*15
pprint (cheques)


opciones = list_powerset(montos)
suma_pago = 0.0
seleccion = ()
for opcion in opciones:
    suma_temporal = sum(opcion)
    if monto_pago >= suma_temporal and (monto_pago-suma_pago)>(monto_pago-suma_temporal):
        suma_pago = suma_temporal
        seleccion = opcion
print suma_pago
print seleccion


print
print "Monto Objetivo:\t\t", round(monto_pago, 2)
print "Monto Obtenido:\t\t", round(suma_pago, 2)
print "Diferencia:\t\t", round(monto_pago - suma_pago, 2)
print "Tolerancia:\t\t", round(ERROR, 2)
print
print "Detalle:"
pprint(seleccion)
elapsed = (time.time() - start)
print
print "Cerca de %s segundos" % round(elapsed, 2)
print
print
print
raw_input("<Enter> para continuar")
print
print

