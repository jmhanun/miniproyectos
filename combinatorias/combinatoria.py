#!/usr/bin/python
# -*- coding: utf-8 -*-

from pprint import pprint
import xlrd
import time

cheques = []
cheques_pago = []
suma_pago = 0
ERROR = 0

book = xlrd.open_workbook("lista_cheques.xls")

sheet = book.sheet_by_name("Sheet1")

print
monto_pago = sheet.row_values(0)[5]
mensaje = "Ingrese monto(default=%s): " % round(monto_pago, 2)
x = raw_input(mensaje)
if x and x != 0:
    monto_pago = float(x)

y = raw_input("Ingrese el error tolerado(default=0): ")
if y and y != 0:
    ERROR = float(y)

#Carga cheques
for row in range(sheet.nrows):
    if sheet.row_values(row)[1] != "":
        cheque = {}
        cheque["monto"] = sheet.row_values(row)[1]
        cheque["orden"] = int(sheet.row_values(row)[0])
        cheque["nro_interno"] = str(sheet.row_values(row)[2])
        cheques.append(cheque)
#Fin carga cheques

start = time.time()
for i in range(1, 2**len(cheques)):
#    print i, type(bin(i).split("b")[1].zfill(len(cheques))), \
#        bin(i).split("b")[1].zfill(len(cheques))
    template = bin(i).split("b")[1].zfill(len(cheques))
    suma_parcial = 0
    cheques_parcial = []
    bandera = False
    for pos, c in enumerate(template):
        if c == '1':
            suma_parcial = suma_parcial + cheques[pos]['monto']
            cheques_parcial.append(cheques[pos])
            if suma_parcial > monto_pago:
                bandera = True
                break
    if not bandera \
          and (monto_pago - suma_pago) > (monto_pago - suma_parcial) \
          and suma_parcial <= monto_pago:
        suma_pago = suma_parcial
        cheques_pago = list(cheques_parcial)
    if abs(monto_pago - suma_parcial) <= ERROR:
        break
print
print "Monto Objetivo:\t\t", round(monto_pago, 2)
print "Monto Obtenido:\t\t", round(suma_pago, 2)
print "Diferencia:\t\t", round(monto_pago - suma_pago, 2)
print "Tolerancia:\t\t", round(ERROR, 2)
print
print "Detalle:"
pprint(cheques_pago)
elapsed = (time.time() - start)
print
print "Cerca de %s segundos" % round(elapsed, 2)
print
print
print
raw_input("<Enter> para continuar")
print
print

