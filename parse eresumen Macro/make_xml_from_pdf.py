
#pdftohtml -c -xml eresumen.pdf prefix


import os


ficheros = os.listdir('./pdfs')

for f in ficheros:
    nombre_pdf = f.replace(' ', r'\ ')
    nombre_xml = ''.join(f.split('.')[:-1])
    nombre_xml = nombre_xml.replace(' ','_') + '.xml'
    comando = 'pdftohtml -c -xml ./pdfs/%s ./xml/%s' % (nombre_pdf, nombre_xml)
    x = os.system(comando)

    if x != 0:
        print "Instale pdftohtml he intente nuevamente"
        exit()

print "*********************************"




