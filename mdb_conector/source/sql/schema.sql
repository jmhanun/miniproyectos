﻿-- ----------------------------------------------------------
-- MDB Tools - A library for reading MS Access database files
-- Copyright (C) 2000-2011 Brian Bruns and others.
-- Files in libmdb are licensed under LGPL and the utilities under
-- the GPL, see COPYING.LIB and COPYING files respectively.
-- Check out http://mdbtools.sourceforge.net
-- ----------------------------------------------------------

SET client_encoding = 'UTF-8';
set schema = "cheques";

CREATE TABLE "Bancos"
 (
	"idBanco"			INTEGER, 
	"nombre"			VARCHAR (100), 
	"alias"			VARCHAR (100)
);
COMMENT ON COLUMN "Bancos"."idBanco" IS 'Identificacion del Banco';
COMMENT ON COLUMN "Bancos"."nombre" IS 'Nombre del Banco';
COMMENT ON COLUMN "Bancos"."alias" IS 'Nombre del Banco reducido';

-- CREATE INDEXES ...
CREATE UNIQUE INDEX "Bancos_idBanco_idx" ON "Bancos" ("idBanco");
ALTER TABLE "Bancos" ADD CONSTRAINT "Bancos_pkey" PRIMARY KEY ("idBanco");

CREATE TABLE "Cheques"
 (
	"idCheque"			VARCHAR (16), 
	"idBanco"			INTEGER, 
	"idSucursal"			INTEGER, 
	"idCuenta"			VARCHAR (100), 
	"fechaPago"			TIMESTAMP WITHOUT TIME ZONE, 
	"monto"			NUMERIC(15,2), 
	"idCuentaInterna"			INTEGER, 
	"añoCuentaInterna"			INTEGER, 
	"nroCuentaInterna"			INTEGER, 
	"idCliente"			INTEGER, 
	"fechaEntrada"			TIMESTAMP WITHOUT TIME ZONE
);
COMMENT ON COLUMN "Cheques"."idCheque" IS 'Identificacion del Cheque (8 caracteres)';
COMMENT ON COLUMN "Cheques"."idBanco" IS 'Identificacion del Banco (3 caracteres)';
COMMENT ON COLUMN "Cheques"."idSucursal" IS 'Identificacion de la Sucursal (3 caracteres)';
COMMENT ON COLUMN "Cheques"."idCuenta" IS 'Identificador de cuenta del banco';
COMMENT ON COLUMN "Cheques"."fechaPago" IS 'Fecha de pago del cheque';
COMMENT ON COLUMN "Cheques"."monto" IS 'Monto del Cheque';
COMMENT ON COLUMN "Cheques"."idCuentaInterna" IS 'Identificacion de la cuenta interna donde esta asentado el cheque';
COMMENT ON COLUMN "Cheques"."añoCuentaInterna" IS 'Año de la cuenta interna';
COMMENT ON COLUMN "Cheques"."nroCuentaInterna" IS 'Numero de la cuenta interna del cheque';
COMMENT ON COLUMN "Cheques"."idCliente" IS 'Identificacion del Cliente';

-- CREATE INDEXES ...
CREATE INDEX "Cheques_ControlInterno_idx" ON "Cheques" ("idCuentaInterna", "añoCuentaInterna", "nroCuentaInterna");
CREATE INDEX "Cheques_idBanco_idx" ON "Cheques" ("idBanco");
CREATE INDEX "Cheques_idCheque_idx" ON "Cheques" ("idCheque");
CREATE INDEX "Cheques_idCliente_idx" ON "Cheques" ("idCliente");
CREATE INDEX "Cheques_idSucursal_idx" ON "Cheques" ("idSucursal");
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_pkey" PRIMARY KEY ("idCheque", "idBanco", "idSucursal", "idCuenta");

CREATE TABLE "ChequesRechazados"
 (
	"idBanco"			INTEGER, 
	"idSucursal"			INTEGER, 
	"idCheque"			VARCHAR (100), 
	"idCuenta"			VARCHAR (100), 
	"idMotivo"			INTEGER
);

-- CREATE INDEXES ...
CREATE INDEX "ChequesRechazados_idBanco_idx" ON "ChequesRechazados" ("idBanco");
CREATE UNIQUE INDEX "ChequesRechazados_idCuenta_idx" ON "ChequesRechazados" ("idCheque");
CREATE UNIQUE INDEX "ChequesRechazados_idCuenta1_idx" ON "ChequesRechazados" ("idCuenta");
CREATE UNIQUE INDEX "ChequesRechazados_idMotivo_idx" ON "ChequesRechazados" ("idMotivo");
CREATE UNIQUE INDEX "ChequesRechazados_idSucursal_idx" ON "ChequesRechazados" ("idSucursal");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_pkey" PRIMARY KEY ("idBanco", "idSucursal", "idCheque", "idCuenta");

CREATE TABLE "Clientes"
 (
	"idCliente"			SERIAL, 
	"nombre"			VARCHAR (100)
);
COMMENT ON COLUMN "Clientes"."idCliente" IS 'Identificacion del Cliente';
COMMENT ON COLUMN "Clientes"."nombre" IS 'Nombre del Cliente';

-- CREATE INDEXES ...
CREATE INDEX "Clientes_idCliente_idx" ON "Clientes" ("idCliente");
CREATE INDEX "Clientes_nombre_idx" ON "Clientes" ("nombre");
ALTER TABLE "Clientes" ADD CONSTRAINT "Clientes_pkey" PRIMARY KEY ("idCliente");

CREATE TABLE "Cuentas"
 (
	"idBanco"			INTEGER, 
	"idSucursal"			INTEGER, 
	"idCuenta"			VARCHAR (100)
);

-- CREATE INDEXES ...
CREATE INDEX "Cuentas_idCuenta_idx" ON "Cuentas" ("idCuenta");
CREATE UNIQUE INDEX "Cuentas_idCuentaBancaria_idx" ON "Cuentas" ("idBanco");
ALTER TABLE "Cuentas" ADD CONSTRAINT "Cuentas_pkey" PRIMARY KEY ("idBanco", "idSucursal", "idCuenta");

CREATE TABLE "CuentasPersonas"
 (
	"idBanco"			INTEGER, 
	"idSucursal"			INTEGER, 
	"idCuenta"			VARCHAR (100), 
	"idNro1"			VARCHAR (4), 
	"idNro2"			VARCHAR (16), 
	"idNro3"			VARCHAR (2)
);

-- CREATE INDEXES ...
CREATE UNIQUE INDEX "CuentasPersonas_idBanco_idx" ON "CuentasPersonas" ("idCuenta");
CREATE INDEX "CuentasPersonas_idBanco1_idx" ON "CuentasPersonas" ("idBanco");
CREATE INDEX "CuentasPersonas_idCheque_idx" ON "CuentasPersonas" ("idNro2");
CREATE INDEX "CuentasPersonas_idCuenta_idx" ON "CuentasPersonas" ("idNro3");
CREATE UNIQUE INDEX "CuentasPersonas_idSucursal_idx" ON "CuentasPersonas" ("idNro1");
CREATE INDEX "CuentasPersonas_idSucursal1_idx" ON "CuentasPersonas" ("idSucursal");
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_pkey" PRIMARY KEY ("idBanco", "idSucursal", "idCuenta", "idNro1", "idNro2", "idNro3");

CREATE TABLE "import"
 (
	"ID"			SERIAL, 
	"banco"			DOUBLE PRECISION, 
	"sucursal"			DOUBLE PRECISION, 
	"cheque"			DOUBLE PRECISION, 
	"cuenta"			VARCHAR (100)
);

-- CREATE INDEXES ...
ALTER TABLE "import" ADD CONSTRAINT "import_pkey" PRIMARY KEY ("ID");

CREATE TABLE "MotivosRechazo"
 (
	"idMotivo"			SERIAL, 
	"descripcion"			VARCHAR (100)
);

-- CREATE INDEXES ...
CREATE INDEX "MotivosRechazo_idMotivo_idx" ON "MotivosRechazo" ("idMotivo");
ALTER TABLE "MotivosRechazo" ADD CONSTRAINT "MotivosRechazo_pkey" PRIMARY KEY ("idMotivo");

CREATE TABLE "Personas"
 (
	"idNro1"			VARCHAR (4), 
	"idNro2"			VARCHAR (16), 
	"idNro3"			VARCHAR (2), 
	"nombre"			VARCHAR (100)
);
COMMENT ON COLUMN "Personas"."idNro1" IS 'Identificacion de la Persona';
COMMENT ON COLUMN "Personas"."idNro2" IS 'Identificacion de la Persona';
COMMENT ON COLUMN "Personas"."idNro3" IS 'Identificacion de la Persona';
COMMENT ON COLUMN "Personas"."nombre" IS 'Nombre de la persona';

-- CREATE INDEXES ...
CREATE INDEX "Personas_idNro_idx" ON "Personas" ("idNro2");
CREATE INDEX "Personas_idNro1_idx" ON "Personas" ("idNro1");
CREATE INDEX "Personas_idNro3_idx" ON "Personas" ("idNro3");
ALTER TABLE "Personas" ADD CONSTRAINT "Personas_pkey" PRIMARY KEY ("idNro1", "idNro2", "idNro3");

CREATE TABLE "Proveedores"
 (
	"idProveedor"			SERIAL, 
	"nombre"			VARCHAR (100)
);

-- CREATE INDEXES ...
CREATE INDEX "Proveedores_idProveedor_idx" ON "Proveedores" ("idProveedor");
ALTER TABLE "Proveedores" ADD CONSTRAINT "Proveedores_pkey" PRIMARY KEY ("idProveedor");

CREATE TABLE "Salidas"
 (
	"idSalida"			SERIAL, 
	"fecha"			TIMESTAMP WITHOUT TIME ZONE, 
	"idTipoSalida"			INTEGER, 
	"idProveedor"			INTEGER, 
	"total"			NUMERIC(15,2)
);

-- CREATE INDEXES ...
CREATE UNIQUE INDEX "Salidas_idMovimiento_idx" ON "Salidas" ("idSalida");
CREATE INDEX "Salidas_idProveedor_idx" ON "Salidas" ("idProveedor");
CREATE INDEX "Salidas_idTipoMovimiento_idx" ON "Salidas" ("idTipoSalida");
ALTER TABLE "Salidas" ADD CONSTRAINT "Salidas_pkey" PRIMARY KEY ("idSalida");

CREATE TABLE "SalidasCheques"
 (
	"idSalida"			INTEGER, 
	"idBanco"			INTEGER, 
	"idSucursal"			INTEGER, 
	"idCuenta"			VARCHAR (100), 
	"idCheque"			VARCHAR (100)
);

-- CREATE INDEXES ...
CREATE UNIQUE INDEX "SalidasCheques_cheques_idx" ON "SalidasCheques" ("idBanco", "idSucursal", "idCuenta", "idCheque");
CREATE UNIQUE INDEX "SalidasCheques_idBanco_idx" ON "SalidasCheques" ("idBanco");
CREATE UNIQUE INDEX "SalidasCheques_idCheque_idx" ON "SalidasCheques" ("idCheque");
CREATE INDEX "SalidasCheques_idCuenta_idx" ON "SalidasCheques" ("idCuenta");
CREATE INDEX "SalidasCheques_idMovimiento_idx" ON "SalidasCheques" ("idSalida");
CREATE INDEX "SalidasCheques_idSucursal_idx" ON "SalidasCheques" ("idSucursal");
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_pkey" PRIMARY KEY ("idSalida", "idBanco", "idSucursal", "idCuenta", "idCheque");

CREATE TABLE "TiposSalidas"
 (
	"idTipoSalida"			SERIAL, 
	"descripcion"			VARCHAR (100)
);

-- CREATE INDEXES ...
CREATE INDEX "TiposSalidas_idTipoMovimiento_idx" ON "TiposSalidas" ("idTipoSalida");
ALTER TABLE "TiposSalidas" ADD CONSTRAINT "TiposSalidas_pkey" PRIMARY KEY ("idTipoSalida");

CREATE TABLE "Sucursales"
 (
	"idSucursal"			INTEGER, 
	"idBanco"			INTEGER, 
	"nombre"			VARCHAR (100), 
	"domicilio"			VARCHAR (200), 
	"cp"			INTEGER
);
COMMENT ON COLUMN "Sucursales"."idSucursal" IS 'Identificacion de la Sucursal del Banco';
COMMENT ON COLUMN "Sucursales"."idBanco" IS 'Identificacion del Banco';
COMMENT ON COLUMN "Sucursales"."nombre" IS 'Nombre de la Sucursal';
COMMENT ON COLUMN "Sucursales"."domicilio" IS 'Domicilio de la Sucursal';
COMMENT ON COLUMN "Sucursales"."cp" IS 'CP de la Sucursal';

-- CREATE INDEXES ...
CREATE UNIQUE INDEX "Sucursales_idBanco_idx" ON "Sucursales" ("idBanco");
CREATE UNIQUE INDEX "Sucursales_idSucursal_idx" ON "Sucursales" ("idSucursal");
CREATE UNIQUE INDEX "Sucursales_orden_idx" ON "Sucursales" ("idBanco", "idSucursal");
ALTER TABLE "Sucursales" ADD CONSTRAINT "Sucursales_pkey" PRIMARY KEY ("idSucursal", "idBanco");


-- CREATE Relationships ...
ALTER TABLE "Sucursales" ADD CONSTRAINT "Sucursales_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Bancos"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idCheque_fk" FOREIGN KEY ("idCheque") REFERENCES "Cheques"("idCheque");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cheques"("idBanco");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cheques"("idSucursal");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cheques"("idCuenta");
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idCheque_fk" FOREIGN KEY ("idCheque") REFERENCES "Cheques"("idCheque") ON UPDATE CASCADE;
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cheques"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cheques"("idSucursal") ON UPDATE CASCADE;
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cheques"("idCuenta") ON UPDATE CASCADE;
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_idCliente_fk" FOREIGN KEY ("idCliente") REFERENCES "Clientes"("idCliente") ON UPDATE CASCADE;
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cuentas"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cuentas"("idSucursal") ON UPDATE CASCADE;
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cuentas"("idCuenta") ON UPDATE CASCADE;
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cuentas"("idBanco");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cuentas"("idSucursal");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cuentas"("idCuenta");
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cuentas"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cuentas"("idSucursal") ON UPDATE CASCADE;
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cuentas"("idCuenta") ON UPDATE CASCADE;
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idMotivo_fk" FOREIGN KEY ("idMotivo") REFERENCES "MotivosRechazo"("idMotivo");
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idNro1_fk" FOREIGN KEY ("idNro1") REFERENCES "Personas"("idNro1") ON UPDATE CASCADE;
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idNro2_fk" FOREIGN KEY ("idNro2") REFERENCES "Personas"("idNro2") ON UPDATE CASCADE;
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idNro3_fk" FOREIGN KEY ("idNro3") REFERENCES "Personas"("idNro3") ON UPDATE CASCADE;
ALTER TABLE "Salidas" ADD CONSTRAINT "Salidas_idProveedor_fk" FOREIGN KEY ("idProveedor") REFERENCES "Proveedores"("idProveedor") ON UPDATE CASCADE;
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idSalida_fk" FOREIGN KEY ("idSalida") REFERENCES "Salidas"("idSalida") ON UPDATE CASCADE;
ALTER TABLE "Cuentas" ADD CONSTRAINT "Cuentas_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Sucursales"("idSucursal") ON UPDATE CASCADE;
ALTER TABLE "Cuentas" ADD CONSTRAINT "Cuentas_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Sucursales"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "Salidas" ADD CONSTRAINT "Salidas_idTipoSalida_fk" FOREIGN KEY ("idTipoSalida") REFERENCES "TiposSalidas"("idTipoSalida") ON UPDATE CASCADE;
