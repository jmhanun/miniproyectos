# -*- coding: iso-8859-15
import sys
import os
import shutil
# from openpyxl import Workbook
# from openpyxl import load_workbook
from os import listdir 
from distutils.util import strtobool

from argparse import ArgumentParser

# argp = ArgumentParser(prog='prueba.py',
#                       description=
#                       """Renombra archivos de forma masiva desde un xls.
#                       Con el parametro -p define el directorio a modificar
#                       Con el parametro -c crea un xls con los nombres de los archivos a modifica
#                       Columna 1: nombre_a_cambiar
#                       Columna 2: nombre_nuevo
#                       """,
#                       usage=
#                       """
# Con el parametro -p define el directorio a modificar
# Con el parametro -c crea un xls con los nombres de los archivos a modifica
# Columna 1: nombre_a_cambiar
# Columna 2: nombre_nuevo

# USO
# ---

# 1. Ejectuar el programa con el argumento -pc indicandole el directorio
#     Esto crea un archivo "renombrar.xls" donde estan todos los nombres
#     de los archivos a renombrar
# 2. Editar el archivo "renombrar.xls"
#     Agregando el nuevo nombre de los archivos en la columna 2 
#     junto al nombre anterior
# 3. Ejectuar nuevamente el programa con el argumento -p indicandole el directorio
#     Esto modifica el nombre archivo indicado en la columna 1
#     por el nombre indicado en la columna 2
#                       """,
#                       epilog='Copyright 2015 Jose Miguel Hanun - GPL v3.0',
#                       version='beta 1.0'
#                       )

# argp.add_argument( '-p', '--path', action='store', required=True, 
#                   help='Nombre del directorio raiz a renombrar',)

# argp.add_argument( '-c', '--create', action='store_true',
#                   help='Crea el xls origen para luego renombrar',)

                  
# args = argp.parse_args()

# def renombrar():
#     mi_path = args.path
#     print mi_path

#     wb = load_workbook(filename = 'renombrar.xlsx')
#     ws = wb.active

#     os.chdir(mi_path)

#     orden = 1

#     rta = user_yes_no_query("\n\nEsta seguro de modificar los archivos del directorio:\n"+mi_path+"?\n")
    
#     if rta:
#         if not ws['B'+str(orden)].value:
#             print "No esta establecido el nombre nuevo..\nActualice 'renombrar.xlsx' y vuelva a intentarlo"
#             return
#         while ws['A'+str(orden)].value:
#             origen = ws['A'+str(orden)].value
#             destino = ws['B'+str(orden)].value
#             orden += 1
#             os.rename(origen, destino)
#         print "{0} archivos renombrados".format(orden-1)
#     else:
#         print "{0} archivos renombrados".format(orden-1)
#         return

    

# def tomar_origenes():
#     mi_path = args.path

#     wb = Workbook()
#     ws = wb.active

#     for orden, archivo in enumerate(os.listdir(mi_path)):
#         ws['A'+str(orden+1)] = archivo
#     wb.save("renombrar.xlsx")


# def user_yes_no_query(question):
#     sys.stdout.write('%s [y/n]\n' % question)
#     while True:
#         try:
#             return strtobool(raw_input().lower())
#         except ValueError:
#             # sys.stdout.write('Please respond with \'y\' or \'n\'.\n')
#             print "Please respond with 'y' or 'n'\n"

import datetime

def encontrar_huecos():
  mi_path = "Envases Pueyrredon"
  huecos = 0
  lista = []
  for orden, archivo in enumerate(os.listdir(mi_path)):
    lista.append(archivo)
  lista.sort()
  dia_desde_siguiente = None
  for archivo in lista:
    nombre_archivo = archivo.split(".")[0]
    desde, hasta = nombre_archivo.split("-")
    dia_desde = datetime.datetime.strptime(desde, "%Y%m%d").date()
    dia_hasta = datetime.datetime.strptime(hasta, "%Y%m%d").date()
    if dia_desde_siguiente:
      if dia_desde != dia_desde_siguiente:
        print(dia_desde, dia_hasta, dia_desde_siguiente)
        huecos += 1
    dia_desde_siguiente = dia_hasta + datetime.timedelta(days=1)
  return huecos

if __name__ == '__main__':
    # if args.create:
    #     tomar_origenes()
    # else:
    #     renombrar()
    huecos = encontrar_huecos()
    if huecos != 0:
      print("Hay {0} huecos".format(huecos))
    else:
      print("Todo OK")
