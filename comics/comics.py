# -*- coding: iso-8859-15
import sys
import os
import shutil
from os import listdir 
import zipfile

from argparse import ArgumentParser

argp = ArgumentParser(prog='comics.py',
                      description=
                      """Comprime todos los directorios de un 'path' 
                      seleccionado en archivos separados.""",
                      epilog='Copyright 2015 Jose Miguel Hanun - GPL v3.0',
                      version='beta 1.0'
                      )

argp.add_argument( '-p', '--path', action='store', required=True, 
                  help='Nombre del directorio raiz a comprimir',)
                  
args = argp.parse_args()
print vars(args)

def extraer():
    if len(sys.argv) < 2:
        print "Este programa necesita un parámetro"
        return None

    mi_path = sys.argv[1]
    
    solo_directorios = []
    for direc in listdir(mi_path):
        solo_directorios.append(os.path.join(mi_path,direc))
    contador = 0
    for dire in solo_directorios:
        for archi in listdir(dire):
            contador += 1
            origen = os.path.join(dire,archi)
            destino = os.path.join(mi_path,archi)
            shutil.copy(origen, destino)
            print "%s archivos copiados... " % contador
    #shutil.copy(mifichero, copiafichero)

def zipear():

    try:
        compression = zipfile.ZIP_DEFLATED
    except:
        compression = zipfile.ZIP_STORED

    modes = { zipfile.ZIP_DEFLATED: 'deflated',
              zipfile.ZIP_STORED:   'stored',
              }

    mi_path = args.path

    solo_directorios = [os.path.join(mi_path,d) 
    for d in os.listdir(mi_path) 
    if os.path.isdir(os.path.join(mi_path, d))]

    for direc in solo_directorios:
        os.chdir(direc)
        nombre_zip = os.path.join(mi_path,os.path.split(direc)[-1]+".zip")
        print "Creando el zip", nombre_zip
        zf = zipfile.ZipFile(nombre_zip, mode='w')
        try:
            for archivo in os.listdir(os.getcwd()):
                print 'adding %s with compression mode %s' % (archivo, modes[compression])
                zf.write(archivo, compress_type=compression)
        finally:
            print 'closing'
            zf.close()
    

if __name__ == '__main__':
    zipear()
