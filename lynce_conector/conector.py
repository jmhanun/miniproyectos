# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 14:16:06 2015

@author: jmhanun
"""

import dbf
import os

from argparse import ArgumentParser

argp = ArgumentParser(prog='conector.py',
                      description=
                      """Muestra la estructura de todos los dbf's contenidos
                      en la carpeta definida como origen (default=dbfs/) y 
                      vuelca los resultados en un archivo (default=reporte_dbfs.txt)""",
                      epilog='Copyright 2015 Jose Miguel Hanun - GPL v3.0',
                      version='beta 1.0'
                      )

argp.add_argument( '-o', '--origen', action='store', required=False, 
                  default="dbfs/", help='carpeta origen de los dbfs',)
argp.add_argument( '-d', '--dest', action='store', required=False, 
                  default="reporte_dbfs.txt", help='archivo destino del reporte de dbfs',)
                  
args = argp.parse_args()



#def reporte_dbfs(origen="dbfs/", dest="reporte_dbfs.txt"):
def reporte_dbfs():

    origen = args.origen
    dest = args.dest


    archivos = [d for d in os.listdir(origen)]
    archivos.sort()

    salida = open(dest, "w")

    for archivo in archivos:
        if len(archivo.split("."))>1 and archivo.split(".")[1] == "DBF":
            tabla = dbf.Table(origen + archivo)
            tabla.open()
            salida.write(str(tabla))
            salida.write(25 * "-.")

    salida.close()


def info_tabla(tabla="dbfs/CLIENTES.DBF"):

    tabla = dbf.Table(tabla)
    tabla.open()
    print tabla


def cantidad_registros_tabla(tabla="dbfs/CLIENTES.DBF"):

    tabla = dbf.Table(tabla)
    tabla.open()
    print len(tabla)


def recorrer_tabla(tabla="dbfs/CLIENTES.DBF"):

    tabla = dbf.Table(tabla)
    tabla.open()
    for record in tabla:
        print record

def sql_sobre_tabla(tabla="dbfs/CLIENTES.DBF", sql="select * where zona == 2"):

    tabla = dbf.Table(tabla)
    tabla.open()
    records = dbf.pql(tabla, sql)
    for record in records:
        print record


def pruebas(tabla="dbfs/ARTISTOC.DBF"):

    tabla = dbf.Table(tabla)
    tabla.open()
    print tabla.structure()
    print 25*"="
    print tabla.field_names


if __name__ == '__main__':
    reporte_dbfs()
