
import xml.etree.ElementTree as ET

from datetime import date
import re

def valid_date(datestring):
    """
    valida un string con el formato dd/mm/aa
    devuelve un datetime.date valido
    en caso de no ser valido devuelve None
    """
    try:
        mat=re.match('(\d{2})[/.-](\d{2})[/.-](\d{2})$', datestring)
        if mat is not None:
            fecha = re.split('[/.-]', datestring)
            return date(int(fecha[2])+2000, int(fecha[1]), int(fecha[0]))
        return None
    except ValueError:
        return None

def valid_monto(montostring):
    """
    valida un string con el formato ###.###,##
    devuelve un float valido
    en caso de no ser valido devuelve None
    """
    try:
        montostring = montostring.replace('.','')
        montostring = montostring.replace(',','.')
        return float(montostring)
    except ValueError:
        return None


#Parsea un xml
tree = ET.parse('eresumen.xml')
root = tree.getroot()

primero = False
data = []

#Recorre el xml en paginas y en texto
for page in root:
    for text in page:
        #Si es un texto valido (distinto de None)
        if text.text is not None:
            #Crea el renglon con datos validos
            renglon = (text.text).split()
            #Si el primer elemento del renglon es una fecha valida 
            #entonces es un renglon del resumen
            #el indice 0 es la fecha
            #el indice -1 es el saldo
            #el indice -2 es el monto
            #el resto es la descripcion
            if valid_date(renglon[0]) is not None:
                primero = True
                fecha = valid_date(renglon[0])
                monto = valid_monto(renglon[-2])
                saldo = valid_monto(renglon[-1])
                descripcion = ' '.join(renglon[1:-2])
                data.append([fecha, descripcion, monto, saldo, ''])
            else:
                #Si aun no encuentra el primer renglon del resumen
                #Toma el valor renglon[-1] que corresponde al saldo inicial
                if not primero:
                    renglon = (text.text).split()
                    saldo_inicial = valid_monto(renglon[-1])

#Recorre la lista y le agrega el tipo de movimiento
#'+' si es credito
#'-' si es debito
for e in data:
    if (int((saldo_inicial + e[2])*100) == int(e[3]*100)):
        e[4] = '+'
    else:
        e[4] = '-'
    saldo_inicial = e[3]


