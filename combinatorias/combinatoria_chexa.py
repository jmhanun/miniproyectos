#!/usr/bin/python
# -*- coding: utf-8 -*-

from pprint import pprint
import xlrd
import xlwt
import time

import multiprocessing as mp
import numpy as np

cheques = []
cheques_pago = []
suma_pago = 0
ERROR = 0

book = xlrd.open_workbook("lista_comp_chexa.xls")

sheet = book.sheet_by_name("Sheet1")



def cargar_cheques(monto_pago):
    cheques = []
    for row in range(sheet.nrows):
        if sheet.row_values(row)[1] != "":
            cheque = {}
            cheque["orden"] = int(sheet.row_values(row)[0])
            cheque["nro_interno"] = int(sheet.row_values(row)[1])
            cheque["monto"] = float(sheet.row_values(row)[2])
            cheque["litros"] = float(sheet.row_values(row)[3])
            cheque["producto"] = str(sheet.row_values(row)[4])
            if cheque["monto"] <= monto_pago:
                cheques.append(cheque)
    return cheques


def seleccion_combinaciones(limite_inf, limite_sup, cheques, suma_pago, output):
    cheques_pago = []
    for i in range(limite_inf, limite_sup):
        #Define en binario cual cheques usar y cual no en cada combinacion
        #ej: 0101 -> usa el segundo y el cuarto cheque en un listado de 4 cheques
        template = bin(i).split("b")[1].zfill(len(cheques))
        suma_parcial = 0
        cheques_parcial = []
        bandera = False
        for pos, c in enumerate(template):
            if c == '1':
                suma_parcial = suma_parcial + cheques[pos]['monto']
                cheques_parcial.append(cheques[pos])
                if suma_parcial > monto_pago:
                    bandera = True
                    break
        if not bandera \
              and (monto_pago - suma_pago) > (monto_pago - suma_parcial) \
              and suma_parcial <= monto_pago:
            suma_pago = suma_parcial
            cheques_pago = list(cheques_parcial)
        if abs(monto_pago - suma_parcial) <= ERROR:
            break
    output.put(cheques_pago)


def sumar_combinacion(cheques_pago):
    total = 0
    for cheque in cheques_pago:
        total += cheque['monto']
    return total

print
monto_pago = sheet.row_values(0)[6]
# mensaje = "Ingrese monto(default=%s): " % round(monto_pago, 2)
# x = raw_input(mensaje)
# if x and x != 0:
#     monto_pago = float(x)
#
# y = raw_input("Ingrese el error tolerado(default=0): ")
# if y and y != 0:
#     ERROR = float(y)

lote = 1024
# z = raw_input("Ingrese el tamaño del lote(default=1024): ")
# if z and z != 0:
#     lote = int(z)

#Carga cheques
cheques = cargar_cheques(monto_pago)
#Fin carga cheques

workbook = xlwt.Workbook(encoding = 'ascii')
worksheet = workbook.add_sheet('Resultados')
offset_col = 0
maxima_cantidad_cheques = 20
#Sacar un subconjunto de cheques.. procesarlo y lo que sobre volver a agregarlo a cheques
while len(cheques) > maxima_cantidad_cheques or sumar_combinacion(cheques) > monto_pago:
    if len(cheques) > maxima_cantidad_cheques :
        subconjunto_cheques = cheques[:maxima_cantidad_cheques]
        cheques = cheques[maxima_cantidad_cheques:]
    else:
        subconjunto_cheques = cheques
        cheques = []


    start = time.time()

    limite_sup = 2**len(subconjunto_cheques)

###############################################################33

    output = mp.Queue()

    processes = []

    for i in range(0,lote):
        processes.append(mp.Process(target=seleccion_combinaciones, args=(i*limite_sup/lote, (i+1)*limite_sup/lote, subconjunto_cheques, suma_pago, output)))


    # Run processes
    print "Run processes"
    for p in processes:
        p.start()

    # Get process results from the output queue
    mejores_combinaciones = [output.get() for p in processes]
    mejores_sumas = [sumar_combinacion(mejores_combinaciones[i]) for i in range(len(mejores_combinaciones))]

    mejor_combinacion = np.argmax(mejores_sumas)

    print
    print "Monto Objetivo:\t\t", round(monto_pago, 2)
    print "Monto Obtenido:\t\t", round(mejores_sumas[mejor_combinacion], 2)
    print "Diferencia:\t\t", round(monto_pago - mejores_sumas[mejor_combinacion], 2)
    print "Tolerancia:\t\t", round(ERROR, 2)
    print
    print "Detalle:"
    pprint(mejores_combinaciones[mejor_combinacion])

    #####################Escribe el xls

    row = 0
    col = 0 + offset_col
    # monto	orden	nro_interno	producto
    worksheet.write(row, col, "comprobante")
    worksheet.write(row, col+1, "monto")
    worksheet.write(row, col+2, "litros")
    worksheet.write(row, col+3, "producto")


    row = 0
    col = 0 + offset_col
    for d in mejores_combinaciones[mejor_combinacion]:
        row += 1
        col = 0 + offset_col
        worksheet.write(row, col, d['nro_interno'])
        worksheet.write(row, col+1, d['monto'])
        worksheet.write(row, col+2, d['litros'])
        worksheet.write(row, col+3, d['producto'])


    offset_col = offset_col + 5
    workbook.save('resultados_chexa.xls')
    ##############################



    print 25*"*-"
    print type(mejores_combinaciones), type(mejores_combinaciones[mejor_combinacion]), type(subconjunto_cheques)
    print len(mejores_combinaciones),len(mejores_combinaciones[mejor_combinacion]), len(subconjunto_cheques)
    print 25*"*-"
    #sacar mejorcombinacion de subconjunto_cheques y agregar subconjunto_cheques a cheques
    subconjunto_cheques = ([i for i in subconjunto_cheques if i not in mejores_combinaciones[mejor_combinacion]])

    cheques = cheques + subconjunto_cheques

    elapsed = (time.time() - start)
    print
    print "Cerca de %s segundos" % round(elapsed, 2)
    print

    # Exit the completed processes
    print "Exit the completed processes"
    for p in processes:
        p.join()

    print "Finish"

print cheques


row = 0
col = 0 + offset_col
# monto	orden	nro_interno	producto
worksheet.write(row, col, "comprobante")
worksheet.write(row, col+1, "monto")
worksheet.write(row, col+2, "litros")
worksheet.write(row, col+3, "producto")


row = 0
col = 0 + offset_col
for d in cheques:
    row += 1
    col = 0 + offset_col
    worksheet.write(row, col, d['nro_interno'])
    worksheet.write(row, col+1, d['monto'])
    worksheet.write(row, col+2, d['litros'])
    worksheet.write(row, col+3, d['producto'])


workbook.save('resultados_chexa.xls')
