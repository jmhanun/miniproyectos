# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 20:03:46 2015

@author: jmhanun
"""

import dbf
import os

from argparse import ArgumentParser

argp = ArgumentParser(prog='conector.py',
                      description=
                      """Muestra la estructura de todos los dbf's contenidos
                      en la carpeta definida como origen (default=dbfs/) y 
                      vuelca los resultados en un archivo (default=reporte_dbfs.txt)""",
                      epilog='Copyright 2015 Jose Miguel Hanun - GPL v3.0',
                      version='beta 1.0'
                      )

argp.add_argument( '-o', '--origen', action='store', required=False, 
                  default="dbfs/", help='carpeta origen de los dbfs',)
argp.add_argument( '-d', '--dest', action='store', required=False, 
                  default="reporte_dbfs.txt", help='archivo destino del reporte de dbfs',)
                  
args = argp.parse_args()


def migrar():
    """Migrar 7 tablas a saber:
    ARTICULO.DBF
        articulo = models.CharField(max_length=13, primary_key=True)
        nombre = models.CharField(max_length=45)
        rubro = models.CharField(max_length=4)
        precventa = models.DecimalField(max_digits=11, decimal_places=4)    
    CLIENTES.DBF
        numero = models.DecimalField(max_digits=5, decimal_places=0, primary_key=True)
        nombre = models.CharField(max_length=30)        
        calle = models.CharField(max_length=30)
        nrocalle = models.CharField(max_length=10)
        barrio = models.CharField(max_length=20)
        localidad = models.CharField(max_length=25)
        codpostal = models.CharField(max_length=10)
        provincia = models.CharField(max_length=1)
        telefono = models.CharField(max_length=50)
        fax = models.CharField(max_length=20)
        nrocuit = models.CharField(max_length=13)
        zona = models.DecimalField(max_digits=2, decimal_places=0)    
    DOCARTIC.DBF
        tipodocume = models.CharField(max_length=1)
        nrodocumen = models.CharField(max_length=13)
        articulo = models.CharField(max_length=13)
        cantidad = models.DecimalField(max_digits=9, decimal_places=2)    
        precioneto = models.DecimalField(max_digits=10, decimal_places=3)    
    DOCUMENT.DBF
        proveoclie = models.CharField(max_length=1)
        numero = models.DecimalField(max_digits=5, decimal_places=0)
        tipodocume = models.CharField(max_length=1)
        nrodocumen = models.CharField(max_length=13)
        fecha = models.DateField()
        detalle = models.CharField(max_length=40)
        asiento = models.DecimalField(max_digits=6, decimal_places=0)
        porciva1 = models.DecimalField(max_digits=5, decimal_places=2)
        porciva2 = models.DecimalField(max_digits=5, decimal_places=2)    
    MOVICAJA.DBF
        tipoasient = models.CharField(max_length=1)
        asiento = models.DecimalField(max_digits=6, decimal_places=0)
        renglon = models.DecimalField(max_digits=3, decimal_places=0)
        recibopago = models.CharField(max_length=1)
        nrorecpago = models.DecimalField(max_digits=5, decimal_places=0)
        fecha = models.DateField()
        fechavenci = models.DateField()
        egresingre = models.CharField(max_length=1)
        importe = models.DecimalField(max_digits=12, decimal_places=2)
    MOVICANC.DBF
        asiento = models.DecimalField(max_digits=6, decimal_places=0)
        renglon = models.DecimalField(max_digits=3, decimal_places=0)
        asiencance = models.DecimalField(max_digits=6, decimal_places=0)
        renglcance = models.DecimalField(max_digits=3, decimal_places=0)
        importe = models.DecimalField(max_digits=12, decimal_places=2)
    PROVEED.DBF
        numero = models.DecimalField(max_digits=5, decimal_places=0, primary_key=True)
        nombre = models.CharField(max_length=30)        
        calle = models.CharField(max_length=30)
        nrocalle = models.CharField(max_length=10)
        barrio = models.CharField(max_length=20)
        localidad = models.CharField(max_length=25)
        codpostal = models.CharField(max_length=10)
        provincia = models.CharField(max_length=1)
        telefono = models.CharField(max_length=50)
        fax = models.CharField(max_length=20)
        nrocuit = models.CharField(max_length=13)
        gastotipo = models.CharField(max_length=1)
    """
    table_names = [
        'ARTICULO.DBF',
        'CLIENTES.DBF',
        'DOCARTIC.DBF',
        'DOCUMENT.DBF',
        'MOVICAJA.DBF',
        'MOVICANC.DBF',
        'PROVEED.DBF',
        ]
    field_names = []

    folder = 'dbfs/'
    salida = open('reporte_reducido.txt', 'w')

    for table_name in table_names:
        table = dbf.Table(folder + table_name).open()
        salida.write(str(table))
        salida.write(25*"-.")
        table.close()
    salida.close()

#def reporte_dbfs(origen="dbfs/", dest="reporte_dbfs.txt"):
def reporte_dbfs():

    origen = args.origen
    dest = args.dest


    archivos = [d for d in os.listdir(origen)]
    archivos.sort()

    salida = open(dest, "w")

    for archivo in archivos:
        if len(archivo.split("."))>1 and archivo.split(".")[1] == "DBF":
            tabla = dbf.Table(origen + archivo)
            tabla.open()
            salida.write(str(tabla))
            salida.write(25 * "-.")

    salida.close()


def info_tabla(tabla="dbfs/CLIENTES.DBF"):

    tabla = dbf.Table(tabla)
    tabla.open()
    print tabla


def cantidad_registros_tabla(tabla="dbfs/CLIENTES.DBF"):

    tabla = dbf.Table(tabla)
    tabla.open()
    print len(tabla)


def recorrer_tabla(tabla="dbfs/CLIENTES.DBF"):

    tabla = dbf.Table(tabla)
    tabla.open()
    for record in tabla:
        print record

def sql_sobre_tabla(tabla="dbfs/CLIENTES.DBF", sql="select * where zona == 2"):

    tabla = dbf.Table(tabla)
    tabla.open()
    records = dbf.pql(tabla, sql)
    for record in records:
        print record


def dist(tabla="dbfs/MOVICAJA.DBF",campo='unidadmedi'):

    tabla = dbf.Table(tabla)
    tabla.open()
    listado = []
    for r in tabla:
        if r[campo] not in listado:
            listado.append(r[campo])
    return listado

def document(tabla="dbfs/MOVICAJA.DBF",campo1='proveoclie',
        campo2='numero',campo3='tipodocume',campo4='nrodocumen'):

    tabla = dbf.Table(tabla)
    tabla.open()
    listado = []
    no_listado = []
    for r in tabla:
        # par = [r[campo1],r[campo2]]
        # par = [r[campo1],r[campo2],r[campo3]]
        par = [r[campo1],r[campo2],r[campo3],r[campo4]]
        if par not in listado:
            listado.append(par)
        else:
            no_listado.append(par)
    print len(listado)
    print len(no_listado)
    return no_listado[0]



if __name__ == '__main__':
    migrar()
