#!/usr/bin/python
# -*- coding: utf-8 -*-

from pprint import pprint
import xlrd
import time

import multiprocessing as mp
import numpy as np

cheques = []
cheques_pago = []
suma_pago = 0
ERROR = 0

book = xlrd.open_workbook("lista_cheques.xls")

sheet = book.sheet_by_name("Sheet1")



def cargar_cheques(monto_pago):
    cheques = []
    for row in range(sheet.nrows):
        if sheet.row_values(row)[1] != "":
            cheque = {}
            cheque["monto"] = sheet.row_values(row)[1]
            cheque["orden"] = int(sheet.row_values(row)[0])
            cheque["nro_interno"] = str(sheet.row_values(row)[2])
            if cheque["monto"] <= monto_pago:
                cheques.append(cheque)
    return cheques


def seleccion_combinaciones(limite_inf, limite_sup, cheques, suma_pago, output):
    cheques_pago = []
    for i in range(limite_inf, limite_sup):
        #Define en binario cual cheques usar y cual no en cada combinacion
        #ej: 0101 -> usa el segundo y el cuarto cheque en un listado de 4 cheques
        template = bin(i).split("b")[1].zfill(len(cheques))
        suma_parcial = 0
        cheques_parcial = []
        bandera = False
        for pos, c in enumerate(template):
            if c == '1':
                suma_parcial = suma_parcial + cheques[pos]['monto']
                cheques_parcial.append(cheques[pos])
                if suma_parcial > monto_pago:
                    bandera = True
                    break
        if not bandera \
              and (monto_pago - suma_pago) > (monto_pago - suma_parcial) \
              and suma_parcial <= monto_pago:
            suma_pago = suma_parcial
            cheques_pago = list(cheques_parcial)
        if abs(monto_pago - suma_parcial) <= ERROR:
            break
    output.put(cheques_pago)


def sumar_combinacion(cheques_pago):
    total = 0
    for cheque in cheques_pago:
        total += cheque['monto']
    return total

print
monto_pago = sheet.row_values(0)[5]
mensaje = "Ingrese monto(default=%s): " % round(monto_pago, 2)
x = raw_input(mensaje)
if x and x != 0:
    monto_pago = float(x)

y = raw_input("Ingrese el error tolerado(default=0): ")
if y and y != 0:
    ERROR = float(y)

lote = 1024
z = raw_input("Ingrese el tamaño del lote(default=1024): ")
if z and z != 0:
    lote = int(z)

#Carga cheques
cheques = cargar_cheques(monto_pago)
#Fin carga cheques

start = time.time()

limite_sup = 2**len(cheques)

#cheques_pago = mejor_combinacion(1, limite_sup, cheques, suma_pago)
#cheques_pago = entendiendo_combinacion(1, limite_sup, cheques, suma_pago)
#suma_pago = sumar_combinacion(cheques_pago)


###############################################################33

output = mp.Queue()

processes = []

for i in range(0,lote):
    processes.append(mp.Process(target=seleccion_combinaciones, args=(i*limite_sup/lote, (i+1)*limite_sup/lote, cheques, suma_pago, output)))


# Run processes
print "Run processes"
for p in processes:
    p.start()
 

# Get process results from the output queue
mejores_combinaciones = [output.get() for p in processes]
mejores_sumas = [sumar_combinacion(mejores_combinaciones[i]) for i in range(len(mejores_combinaciones))]

mejor_combinacion = np.argmax(mejores_sumas)
# menor_diferencia = suma_pago

# for i in range(len(mejores_combinaciones)):
#     if suma_pago-mejores_sumas[i] < menor_diferencia:
#         mejor_combinacion = i
#         menor_diferencia = suma_pago-mejores_sumas[i]

######################################################################

print
print "Monto Objetivo:\t\t", round(monto_pago, 2)
print "Monto Obtenido:\t\t", round(mejores_sumas[mejor_combinacion], 2)
print "Diferencia:\t\t", round(monto_pago - mejores_sumas[mejor_combinacion], 2)
print "Tolerancia:\t\t", round(ERROR, 2)
print
print "Detalle:"
pprint(mejores_combinaciones[mejor_combinacion])
elapsed = (time.time() - start)
print
print "Cerca de %s segundos" % round(elapsed, 2)
print
print
print
raw_input("<Enter> para continuar")
print
print


# Exit the completed processes
print "Exit the completed processes"
for p in processes:
    p.join()

print "Finish"
