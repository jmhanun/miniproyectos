import os
import sys
from pprint import pprint
import xml.etree.ElementTree as ET

## Abre el archivo transicion.txt
## Es la exportacion del macrosueldos
## Relaciona nros de legajos y nros de dni
#transicion = 'transicion.txt'
#quitar maria de los angeles del listado porque se le paga en efectivo
transicion = 'transicion_menos_angeles.txt'
f = open(transicion, 'r')
legajos = {}
for line in f:
    legajos[int(line.split()[0])] = line.split()[1]
f.close()

## Abre el archivo todos.txt
## Es la exportacion del macrosueldos
## Es el formato para el ingreso de los datos del tango sueldos al macrosueldo
#todos = 'todos.txt'
#quitar maria de los angeles del listado porque se le paga en efectivo
todos = 'todos_menos_angeles.txt'
f = open(todos, 'r')
origen = {}
for line in f:
    origen[line[14:22]] = line[:-2]
f.close()


## Abre los archivos xml que haya en el directorio
ficheros = os.listdir('.')
archivos_xml = []
for f in ficheros:
    if f[-3:].upper() == "XML":
        archivos_xml.append(f)
        
for archivo_xml in archivos_xml:
    nombre = archivo_xml.split('.')[0]

    try:
        tree = ET.parse(archivo_xml)
        root = tree.getroot()

        total_macro = 0.0
        total_tango = 0.0
        
        ##Recorre los empleados que hay en el xml
        for empleado in root[1][0]:
            legajo = int(empleado.get('LEGAJO'))
            monto = empleado.get('TOTAL_NETO')
            total_tango = total_tango + float(monto)
            ##Si el legajo que viene del xml tiene una coincidencia en macrosueldo
            if legajo in legajos:
                dni = legajos[legajo]
                nueva_linea = origen[dni]
                pesos = monto.split('.')[0]
                print pesos            
                if len(monto.split('.')) > 1:
                    centavos = monto.split('.')[1]
                else:
                    centavos = '0'
                if len(centavos) == 1:
                    centavos = centavos + '0'
                nueva_linea = origen[dni][:-10] + '0'*(10-len(pesos)-len(centavos))+ pesos + centavos
                nuevo = open(nombre + '.txt','a')
                nuevo.write(nueva_linea+'\n')
                nuevo.close()
                total_macro = total_macro + float(monto)

        print
        print 'Verificar el total en Tango:', total_tango
        print 'Verificar el total en Macro:', total_macro
        print
    except:
        print 'Error inesperado:', sys.exc_info()[0]

raw_input('Presione cualquier tecla para terminar.')
