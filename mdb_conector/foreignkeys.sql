BEGIN;

SET search_path = "cheques", pg_catalog;

ALTER TABLE "Sucursales" ADD CONSTRAINT "Sucursales_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Bancos"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idCheque_fk" FOREIGN KEY ("idCheque") REFERENCES "Cheques"("idCheque");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cheques"("idBanco");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cheques"("idSucursal");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cheques"("idCuenta");
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idCheque_fk" FOREIGN KEY ("idCheque") REFERENCES "Cheques"("idCheque") ON UPDATE CASCADE;
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cheques"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cheques"("idSucursal") ON UPDATE CASCADE;
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cheques"("idCuenta") ON UPDATE CASCADE;
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_idCliente_fk" FOREIGN KEY ("idCliente") REFERENCES "Clientes"("idCliente") ON UPDATE CASCADE;
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cuentas"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cuentas"("idSucursal") ON UPDATE CASCADE;
ALTER TABLE "Cheques" ADD CONSTRAINT "Cheques_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cuentas"("idCuenta") ON UPDATE CASCADE;
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cuentas"("idBanco");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cuentas"("idSucursal");
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cuentas"("idCuenta");
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Cuentas"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Cuentas"("idSucursal") ON UPDATE CASCADE;
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idCuenta_fk" FOREIGN KEY ("idCuenta") REFERENCES "Cuentas"("idCuenta") ON UPDATE CASCADE;
ALTER TABLE "ChequesRechazados" ADD CONSTRAINT "ChequesRechazados_idMotivo_fk" FOREIGN KEY ("idMotivo") REFERENCES "MotivosRechazo"("idMotivo");
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idNro1_fk" FOREIGN KEY ("idNro1") REFERENCES "Personas"("idNro1") ON UPDATE CASCADE;
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idNro2_fk" FOREIGN KEY ("idNro2") REFERENCES "Personas"("idNro2") ON UPDATE CASCADE;
ALTER TABLE "CuentasPersonas" ADD CONSTRAINT "CuentasPersonas_idNro3_fk" FOREIGN KEY ("idNro3") REFERENCES "Personas"("idNro3") ON UPDATE CASCADE;
ALTER TABLE "Salidas" ADD CONSTRAINT "Salidas_idProveedor_fk" FOREIGN KEY ("idProveedor") REFERENCES "Proveedores"("idProveedor") ON UPDATE CASCADE;
ALTER TABLE "SalidasCheques" ADD CONSTRAINT "SalidasCheques_idSalida_fk" FOREIGN KEY ("idSalida") REFERENCES "Salidas"("idSalida") ON UPDATE CASCADE;
ALTER TABLE "Cuentas" ADD CONSTRAINT "Cuentas_idSucursal_fk" FOREIGN KEY ("idSucursal") REFERENCES "Sucursales"("idSucursal") ON UPDATE CASCADE;
ALTER TABLE "Cuentas" ADD CONSTRAINT "Cuentas_idBanco_fk" FOREIGN KEY ("idBanco") REFERENCES "Sucursales"("idBanco") ON UPDATE CASCADE;
ALTER TABLE "Salidas" ADD CONSTRAINT "Salidas_idTipoSalida_fk" FOREIGN KEY ("idTipoSalida") REFERENCES "TiposSalidas"("idTipoSalida") ON UPDATE CASCADE;

END;
